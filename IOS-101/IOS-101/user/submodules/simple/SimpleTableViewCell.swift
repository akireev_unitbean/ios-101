//
//  SimpleTableViewCell.swift
//  IOS-101
//
//  Created by artem.kireev@unitbean on 20/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import UIKit

class SimpleTableViewCell: UITableViewCell {
    
    static let nibName: String = "SimpleTableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var valueLabel: UILabel!
    
    func customize(title: String?, value: String?) {
        titleLabel.text = title
        valueLabel.text = value
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
