//
//  PhotoTableViewCell.swift
//  IOS-101
//
//  Created by artem.kireev@unitbean on 19/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import UIKit

class PhotoTableViewCell: UITableViewCell {
    
    static let nibName: String = "PhotoTableViewCell"
    
    @IBOutlet weak var photoImageView: UIImageView!
    
    func customize(with photo: UserPhoto) {
        let photoSize = photo.size
        let cellSize = bounds.size
        let scale = cellSize.width / photoSize.width
        let size = CGSize(width: cellSize.width, height: photoSize.height * scale)
        let frame = CGRect(origin: CGPoint.zero, size: size)
        photoImageView.image = photo
        photoImageView.frame = frame
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
