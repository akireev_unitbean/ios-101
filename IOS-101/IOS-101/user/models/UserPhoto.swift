//
//  UserPhoto.swift
//  IOS-101
//
//  Created by artem.kireev@unitbean on 20/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import UIKit


typealias UserPhoto = UIImage
