//
//  UserModel.swift
//  IOS-101
//
//  Created by artem.kireev@unitbean on 19/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//
import Foundation
import ObjectMapper


class UserModel: NSObject, Mappable {
    
    var id: Int?
    var name: String?
    var username: String?
    var email: String?
    var addressModel: AddressModel?
    var phone: String?
    var website: String?
    var company: CompanyModel?
    
    required init?(map: Map) {
        super.init()
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        username <- map["username"]
        email <- map["email"]
        addressModel <- map["address"]
        phone <- map["phone"]
        website <- map["website"]
        company <- map["company"]
    }
    
}


class AddressModel: Mappable {
    
    var street: String?
    var suite: String?
    var city: String?
    var zipcode: String?
    var geoposition: GeopositionModel?
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        street <- map["street"]
        suite <- map["suite"]
        city <- map["city"]
        zipcode <- map["zipcode"]
        geoposition <- map["geo"]
    }
    
}


class GeopositionModel: Mappable {
    
    var latitude: String?
    var longitude: String?
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        latitude <- map["lat"]
        longitude <- map["lng"]
    }
    
}


class CompanyModel: Mappable {
    
    var name: String?
    var catchPhrase: String?
    var bs: String?
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        catchPhrase <- map["catchPhrase"]
        bs <- map["bs"]
    }
    
}
