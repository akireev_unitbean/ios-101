//
//  UserService.swift
//  IOS-101
//
//  Created by artem.kireev@unitbean on 19/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import Alamofire
import ObjectMapper


typealias UserInfoCompletion = (_ user: UserModel?, _ error: String?) -> Void


class UserInfoService: TypicodeApiService {
    
    func userInfo(userId: Int, completion: @escaping UserInfoCompletion) {
        let url = host + "/users/" + String(userId)
        
        self.sendRequestWithJSONResponse(
            requestType: .get,
            url: url,
            params: nil,
            headers: nil,
            paramsEncoding: URLEncoding.default
        ) { (json, error) in
            guard error == nil else {
                completion(nil, error!.localizedDescription)
                return
            }
            guard let user = json!.dictionaryObject else {
                completion(nil, "Loading error")
                return
            }
            let userModel = Mapper<UserModel>().map(JSONObject: user)
            completion(userModel, nil)
        }
    }
    
}
