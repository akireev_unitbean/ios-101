//
//  UserPhotoService.swift
//  IOS-101
//
//  Created by artem.kireev@unitbean on 20/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import Alamofire

typealias UserPhotoCompletion = (_ userPhoto: UserPhoto?, _ errorMessage: String?) -> Void


class UserPhotoService: DataApiService {
    
    func userPhoto(forUserWith id: Int, completion: @escaping UserPhotoCompletion) {
        let url = "http://www.photoforum.ru/f/photo/000/552/552361_35.jpg"
        self.sendRequestWithDataResponse(
            requestType: .get,
            url: url,
            params: nil,
            headers: nil,
            paramsEncoding: URLEncoding.default
        ) { (data, error) in
            guard error == nil else {
                completion(nil, error!.localizedDescription)
                return
            }
            guard let userPhoto = UserPhoto(data: data!) else {
                completion(nil, "Cannot convert downloaded data to uiimage")
                return
            }
            completion(userPhoto, nil)
        }
    }
    
}
