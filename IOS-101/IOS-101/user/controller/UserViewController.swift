//
//  UserViewController.swift
//  IOS-101
//
//  Created by artem.kireev@unitbean on 19/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import UIKit


class UserViewController: UIViewController, UserDataProviderDelegate, UITableViewDelegate, UITableViewDataSource {
   
    static let nibName: String = "UserViewController"
    
    var dataProvider: UserDataProvider = UserDataProvider()
    
    var userId: Int? {
        get {
            return dataProvider.userId
        }
        set {
            dataProvider.userId = newValue
        }
    }
    
    
    // MARK: Controller
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeTableView()
        dataProvider.delegate = self
        dataProvider.load()
    }
    
    
    // MARK: Table
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            customizeTableView()
        }
    }
    
    func customizeTableView() {
        guard let tableView = tableView else {
            // tableView hasn't been set yet
            return
        }
        let foo = [PhotoTableViewCell.nibName, SimpleTableViewCell.nibName]
        for i in foo {
            let nib = UINib(nibName: i, bundle: nil)
            tableView.register(nib, forCellReuseIdentifier: i)
        }
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return _viewModel.sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _viewModel.sections[section].rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellModel = _viewModel.sections[indexPath.section].rows[indexPath.row]
        let cell: UITableViewCell
        switch cellModel {
        case .photo(let photo):
            cell = tableView.dequeueReusableCell(withIdentifier: PhotoTableViewCell.nibName, for: indexPath)
            let photoCell = cell as! PhotoTableViewCell
            photoCell.customize(with: photo)
        case .simple(let data):
            cell = tableView.dequeueReusableCell(withIdentifier: SimpleTableViewCell.nibName, for: indexPath)
            let simpleCell = cell as! SimpleTableViewCell
            simpleCell.customize(title: data.title, value: data.value)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellModel = _viewModel.sections[indexPath.section].rows[indexPath.row]
        switch cellModel {
        case .photo(let photo):
            let scale = tableView.bounds.width / photo.size.width
            let height = photo.size.height * scale
            return height
        case .simple:
            return UITableViewAutomaticDimension
        }
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellModel = _viewModel.sections[indexPath.section].rows[indexPath.row]
        switch cellModel {
        case .photo:
            return 400
        case .simple:
            return 80
        }
    }
    
    
    // MARK: - UserDataProviderDelegate
    
    func dataProvider(_ dataProvider: UserDataProvider, didLoad userModel: UserModel) {
        _viewModel = _ViewModel.from(model: userModel, photo: dataProvider.userPhoto)
    }
    
    func dataProvider(_ dataProvider: UserDataProvider, didLoad userPhoto: UserPhoto) {
        _viewModel = _ViewModel.from(model: dataProvider.userModel, photo: userPhoto)
    }

    
    func dataProvider(_ dataProvider: UserDataProvider, didReceived error: String) {
        // TODO show error
        fatalError("\(error)")
    }
    
    
    // MARK: Private
    
    private var _viewModel: _ViewModel = _ViewModel(title: "@Username", sections: []) {
        didSet {
            _updateView()
        }
    }
    
    private func _updateView() {
        title = _viewModel.title
        tableView.reloadData()
    }

}


// MARK: Private

private enum _RowByField: Int {
    case name = 0
    case email
    case address
    case phone
    case website
    case company
}


private struct _ViewModel {
    let title: String
    let sections: [_Section]
    
    static func from(model: UserModel?, photo: UserPhoto?) -> _ViewModel {
        var sections: [_Section] = []
        if let photo = photo {
            sections.append(_Section(rows: [
                _Row.photo(photo),
            ]))
        }
        if let model = model {
            sections.append(_Section(rows: [
                _Row.makeSimple(title: "Name", value: model.name),
                _Row.makeSimple(title: "Email", value: model.email),
                _Row.makeSimple(title: "City", value: model.addressModel!.city),
                _Row.makeSimple(title: "Phone", value: model.phone),
                _Row.makeSimple(title: "Website", value: model.website),
                _Row.makeSimple(title: "Company", value: model.company!.name),
            ]))
        }
        return _ViewModel(title: model?.username ?? "Unknown Username", sections: sections)
    }
    
}


private struct _Section {
    let rows: [_Row]
}


private enum _Row {
    case simple(_SimpleRowData)
    case photo(UserPhoto)
    
    static func makeSimple(title: String, value: String?) -> _Row {
        return _Row.simple(_SimpleRowData(title: title, value: value))
    }
    
}


private struct _SimpleRowData {
    let title: String
    let value: String?
}
