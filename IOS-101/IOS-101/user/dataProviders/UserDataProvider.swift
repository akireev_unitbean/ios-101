//
//  UserDataProvider.swift
//  IOS-101
//
//  Created by artem.kireev@unitbean on 19/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import Foundation


protocol UserDataProviderDelegate: class {
    func dataProvider(_ dataProvider: UserDataProvider, didLoad userModel: UserModel)
    func dataProvider(_ dataProvider: UserDataProvider, didLoad userPhoto: UserPhoto)
    func dataProvider(_ dataProvider: UserDataProvider, didReceived error: String)
}


class UserDataProvider {
    
    var userId: Int?
    
    var userModel: UserModel?
    
    var userPhoto: UserPhoto?
    
    weak var delegate: UserDataProviderDelegate?
    
    func load() {
        guard let id = userId else {
            return
        }
        SharedApiService.shared.userInfoService.userInfo(userId: id) { (model, errorMessage) in
            guard errorMessage == nil else {
                self.delegate?.dataProvider(self, didReceived: errorMessage!)
                return
            }
            guard let model = model else {
                fatalError("Both userModel and errorMessage are nil")
            }
            self.userModel = model
            self.delegate?.dataProvider(self, didLoad: model)
        }
        SharedApiService.shared.userPhotoService.userPhoto(forUserWith: id) { (photo, errorMessage) in
            guard errorMessage == nil else {
                self.delegate?.dataProvider(self, didReceived: errorMessage!)
                return
            }
            guard let photo = photo else {
                fatalError("Both photo and errorMessage are nil")
            }
            self.userPhoto = photo
            self.delegate?.dataProvider(self, didLoad: photo)
        }
    }
    
}
