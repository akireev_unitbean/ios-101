//
//  ArticleTitleTableViewCell.swift
//  IOS-101
//
//  Created by Artem Kireev on 16/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import UIKit

class ArticleTitleTableViewCell: UITableViewCell {
    
    static let nibName: String = "ArticleTitleTableViewCell"
        
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    func customize(title: String?, date: String?) {
        titleLabel.text = title
        dateLabel.text = date
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
