//
//  CommentTableViewCell.swift
//  IOS-101
//
//  Created by Artem Kireev on 16/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import UIKit


typealias TapOnUserCallback = (CommentTableViewCell) -> Void


class CommentTableViewCell: UITableViewCell {
    
    static let nibName: String = "CommentTableViewCell"
    
    private(set) var onTapUser: TapOnUserCallback?
    
    @IBOutlet weak var usernameLabel: UILabel! {
        didSet {
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(_action))
            usernameLabel.addGestureRecognizer(tapGestureRecognizer)
        }
    }
    
    @IBOutlet weak var userImage: UIImageView! {
        didSet {
            let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(_action))
            userImage.addGestureRecognizer(tapGestureRecognizer)
        }
    }

    @IBOutlet weak var commentLabel: UILabel!

    @IBOutlet weak var dateLabel: UILabel!
    
    func customize(username: String?, date: String?, comment: String?, onTapUserCallback: @escaping TapOnUserCallback) {
        usernameLabel.text = username
        dateLabel.text = date
        commentLabel.text = comment
        onTapUser = onTapUserCallback
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // MARK: Privat
    
    @objc private func _action(_ sender: UIGestureRecognizer) {
        guard sender.state == .ended else {
            return
        }
        onTapUser?(self)
    }

}
