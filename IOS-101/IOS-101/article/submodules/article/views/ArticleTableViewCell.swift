//
//  ArticleTableViewCell.swift
//  IOS-101
//
//  Created by Artem Kireev on 16/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {
    
    static let nibName: String = "ArticleTableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var bodyLabel: UILabel!
    
    func customize(article: ArticleModel) {
        titleLabel.text = article.title
        bodyLabel.text = article.body
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
