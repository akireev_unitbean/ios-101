//
//  ArticleContentTableViewCell.swift
//  IOS-101
//
//  Created by Artem Kireev on 16/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import UIKit

class ArticleContentTableViewCell: UITableViewCell {
    
    static let nibName: String = "ArticleContentTableViewCell"
        
    @IBOutlet weak var contentLabel: UILabel!
    
    func customize(content: String?) {
        contentLabel.text = content
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
