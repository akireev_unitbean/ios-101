//
//  AllArticlesViewController.swift
//  IOS-101
//
//  Created by Artem Kireev on 16/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import UIKit

class AllArticlesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, AllArticlesDataProviderDelegate {
    
    var dataProvider: AllArticlesDataProvider = AllArticlesDataProvider()
    
    
    // MARK: CONTROLLER

    override func viewDidLoad() {
        super.viewDidLoad()
        customizeTableView()
        dataProvider.delegate = self
        dataProvider.refresh()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: TABLE
    
    @IBOutlet weak var tableView: UITableView!
    
    func customizeTableView() {
        let nib = UINib(nibName: ArticleTableViewCell.nibName, bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: ArticleTableViewCell.nibName)
        tableView.estimatedRowHeight = 344
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataProvider.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ArticleTableViewCell.nibName, for: indexPath) as! ArticleTableViewCell
        cell.customize(article: dataProvider.articles[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let article = dataProvider.articles[indexPath.row]
        ArticleRoutes.showArticle(article, from: self)
    }
    
    
    // MARK: AllArticlesDataProviderDelegates
    
    func dataProvider(_ dataProvider: AllArticlesDataProvider, didLoad articles: [ArticleModel]) {
        tableView.reloadData()
    }
    
    func dataProvider(_ dataProvider: AllArticlesDataProvider, didReceived error: String) {
        // TODO show error
    }

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
