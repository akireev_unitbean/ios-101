//
//  ArticleTableViewController.swift
//  IOS-101
//
//  Created by Artem Kireev on 16/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import UIKit

class ArticleTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CommentsDataProviderDelegate {
    
    static let nibName: String = "ArticleTableViewController"
    
    var article: ArticleModel {
        get {
            return dataProvider.article
        }
        set {
            dataProvider.article = newValue
        }
    }
    
    var dataProvider: CommentsDataProvider = CommentsDataProvider()

    
    // MARK: CONTROLLER

    override func viewDidLoad() {
        super.viewDidLoad()
        customizeTableView()
        dataProvider.delegate = self
        dataProvider.refresh()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: TABLE
    
    @IBOutlet weak var tableView: UITableView! {
        didSet {
            tableView.estimatedRowHeight = 80
            tableView.rowHeight = UITableViewAutomaticDimension
        }
    }
    
    func customizeTableView() {
        let foo: [String] = [
            ArticleContentTableViewCell.nibName,
            ArticleTitleTableViewCell.nibName,
            CommentTableViewCell.nibName
        ]
        for i in foo {
            tableView?.register(UINib(nibName: i, bundle: nil), forCellReuseIdentifier: i)
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 2 {
            return dataProvider.comments.count
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
        let article = dataProvider.article
        switch indexPath.section {
        case 0:
            cell = tableView.dequeueReusableCell(withIdentifier: ArticleTitleTableViewCell.nibName, for: indexPath)
            (cell as! ArticleTitleTableViewCell).customize(title: article.title, date: "date")
        case 1:
            cell = tableView.dequeueReusableCell(withIdentifier: ArticleContentTableViewCell.nibName, for: indexPath)
            (cell as! ArticleContentTableViewCell).customize(content: article.body)
        case 2:
            cell = tableView.dequeueReusableCell(withIdentifier: CommentTableViewCell.nibName, for: indexPath)
            let comment = dataProvider.comments[indexPath.row]
            let commentCell = cell  as! CommentTableViewCell
            commentCell.customize(username: comment.name, date: "date", comment: comment.body) { [weak self] (cell) in
                guard let indexPath = self?.tableView.indexPath(for: cell) else {
                    return
                }
                guard let id = self?.dataProvider.comments[indexPath.row].id else {
                    return
                }
                ArticleRoutes.showUser(with: id, from: self!)
            }
        default:
            fatalError()
        }
        return cell
    }
    
    
    // MARK: :CommentsDataProviderDelegate
    
    func dataProvider(_ dataProvider: CommentsDataProvider, didLoad comments: [CommentModel]) {
        tableView.reloadData()
    }
    
    func dataProvider(_ dataProvider: CommentsDataProvider, didReceived error: String) {
        // TODO show error
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
