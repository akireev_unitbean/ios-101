//
//  CommentsDataProvider.swift
//  IOS-101
//
//  Created by Artem Kireev on 16/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import Foundation


protocol CommentsDataProviderDelegate: class {
    func dataProvider(_ dataProvider: CommentsDataProvider, didLoad comments: [CommentModel])
    func dataProvider(_ dataProvider: CommentsDataProvider, didReceived error: String)
}


class CommentsDataProvider: NSObject {
    
    var article: ArticleModel = ArticleModel()
    
    private(set) var comments: [CommentModel] = []
    
    weak var delegate: CommentsDataProviderDelegate?
    
    func refresh() {
        comments = []
        load()
    }
    
    func load() {
        guard let id = article.id else {
            return
        }
        SharedApiService.shared.commentsService.comments(articleId: id) { [weak self] (comments, errorMessage) in
            guard errorMessage == nil else {
                self?.delegate?.dataProvider(self!, didReceived: errorMessage!)
                return
            }
            guard let comments = comments else {
                fatalError("Both comments and errorMessage are nil")
            }
            self?.comments.append(contentsOf: comments)
            self?.delegate?.dataProvider(self!, didLoad: comments)
        }
    }
    
}
