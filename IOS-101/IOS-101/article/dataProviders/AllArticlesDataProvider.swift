//
//  ArticleDataProvider.swift
//  IOS-101
//
//  Created by Artem Kireev on 16/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import Foundation


protocol AllArticlesDataProviderDelegate: class {
    func dataProvider(_ dataProvider: AllArticlesDataProvider, didLoad articles: [ArticleModel])
    func dataProvider(_ dataProvider: AllArticlesDataProvider, didReceived error: String)
}


class AllArticlesDataProvider: NSObject {
    
    private(set) var articles: [ArticleModel] = []
    
    weak var delegate: AllArticlesDataProviderDelegate?
    
    func refresh() {
        articles = []
        self.load()
    }
    
    func load() {
        SharedApiService.shared.articlesService.posts(limit: 100, skip: 100) { [weak self] (articles, errorMessage) in
            guard errorMessage == nil else {
                self?.delegate?.dataProvider(self!, didReceived: errorMessage!)
                return
            }
            guard let articles = articles else {
                fatalError("Both articles and errorMessage is nil")
            }
            self?.articles.append(contentsOf: articles)
            self?.delegate?.dataProvider(self!, didLoad: articles)
        }
    }
    
}
