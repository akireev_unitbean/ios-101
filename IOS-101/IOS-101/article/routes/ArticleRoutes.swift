//
//  ArticleRoutes.swift
//  IOS-101
//
//  Created by Artem Kireev on 16/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import UIKit


class ArticleRoutes {
    
    static func showArticle(_ article: ArticleModel, from vc: UIViewController) {
        let destination = UIStoryboard(name: "Article", bundle: nil).instantiateViewController(withIdentifier: ArticleTableViewController.nibName) as! ArticleTableViewController
        destination.article = article
        destination.customizeTableView()
        vc.navigationController?.pushViewController(destination, animated: true)
    }
    
    static func showUser(with id: Int, from vc: UIViewController) {
        let name = UserViewController.nibName
        let destination = UIStoryboard(name: name, bundle: nil).instantiateViewController(withIdentifier: name) as! UserViewController
        destination.userId = id
        destination.customizeTableView()
        vc.navigationController?.pushViewController(destination, animated: true)
    }
    
}
