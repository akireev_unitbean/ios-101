//
//  CommentsService.swift
//  IOS-101
//
//  Created by Artem Kireev on 16/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper


typealias ArticleCommentsCompletion = (_ comments: [CommentModel]?, _ error: String?) -> Void


class CommentService: TypicodeApiService {
    
    func comments(articleId: Int, completion: @escaping ArticleCommentsCompletion) {
        let url = host + "/comments"
        
        var params: [String:Any] = [:]
        params["postId"] = articleId
        
        self.sendRequestWithJSONResponse(requestType: HTTPMethod.get, url: url, params: params, headers: nil, paramsEncoding: URLEncoding.default) { (json, error) in
            
            guard error == nil else {
                completion(nil, error!.localizedDescription)
                return
            }
            guard let comments = json!.arrayObject else {
                completion(nil, "Loading error")
                return
            }
            let commentModels = Mapper<CommentModel>().mapArray(JSONObject: comments)
            completion(commentModels, nil)
        }
    }
    
}
