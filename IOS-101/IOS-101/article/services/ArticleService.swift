//
//  ArticleService.swift
//  IOS-101
//
//  Created by Artem Kireev on 16/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper


typealias ArticlesPostsCompletion = (_ articles: [ArticleModel]?, _ error: String?) -> Void

class ArticleService: TypicodeApiService {
    
    func posts(limit: Int, skip: Int, completion: @escaping ArticlesPostsCompletion) {
        let url = host + "/posts"
        
        var params: [String:Any] = [:]
        params["limit"] = limit
        params["skip"] = skip
        
        self.sendRequestWithJSONResponse(
            requestType: HTTPMethod.get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default) { (json, error) in
                
            guard error == nil else {
                completion(nil, error!.localizedDescription)
                return
            }
            guard let articles = json!.arrayObject else {
                completion(nil, "Loading error")
                return
            }
            let articleModels = Mapper<ArticleModel>().mapArray(JSONObject: articles)
            completion(articleModels, nil)
        }
    }
    
}
