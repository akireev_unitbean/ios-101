//
//  UIImageApiService.swift
//  IOS-101
//
//  Created by artem.kireev@unitbean on 20/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import Alamofire


typealias RequestDataCompetion = (_ data: Data?, _ error: Error?) -> Void


class DataApiService: AlamofireApiService {
    
    func sendRequestWithDataResponse(
        requestType: HTTPMethod,
        url: String,
        params: [String:Any]?,
        headers: HTTPHeaders?,
        paramsEncoding: ParameterEncoding,
        completion: @escaping RequestDataCompetion
        ) {
        
        debugPrint(params ?? "nil")
        let requestHeaders = headers ?? HTTPHeaders()
        debugPrint(requestHeaders)
        
        let request = APIManager.shared.request(
            url as URLConvertible,
            method: requestType,
            parameters: params,
            encoding: paramsEncoding,
            headers: requestHeaders
        )
        
        request.response { (dataResponce) in
            debugPrint(dataResponce)
            guard dataResponce.error == nil else {
                completion(nil, dataResponce.error!)
                return
            }
            guard let data = dataResponce.data else {
                fatalError("Both data and error are nil")
            }
            completion(data, nil)
        }
    }
    
}
