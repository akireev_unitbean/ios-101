//
//  AlamofireApiService.swift
//  IOS-101
//
//  Created by artem.kireev@unitbean on 20/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import Alamofire


class AlamofireApiService: NSObject {
    
    struct APIManager {
        static let shared: SessionManager = {
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 20
            return SessionManager(configuration: configuration)
        }()
    }

}
