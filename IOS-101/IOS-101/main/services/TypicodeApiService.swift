//
//  TypicodeApiService.swift
//  IOS-101
//
//  Created by Artem Kireev on 16/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


typealias RequestJSONCompletion = (_ results: JSON?, _ error: Error?) -> Void

class TypicodeApiService: AlamofireApiService {
    
//    struct APIManager {
//        static let shared: SessionManager = {
//            let configuration = URLSessionConfiguration.default
//            configuration.timeoutIntervalForRequest = 20
//            return SessionManager(configuration: configuration)
//        }()
//    }
    
    let host = "https://jsonplaceholder.typicode.com"
    
    func sendRequestWithJSONResponse(
        requestType: HTTPMethod,
        url: String,
        params: [String:Any]?,
        headers: HTTPHeaders?,
        paramsEncoding: ParameterEncoding,
        completion: @escaping RequestJSONCompletion
        ) {
        
        debugPrint(params ?? "nil")
        let requestHeaders = headers ?? HTTPHeaders()
        debugPrint(requestHeaders)
        
        let request = APIManager.shared.request(
            url as URLConvertible,
            method: requestType,
            parameters: params,
            encoding: paramsEncoding,
            headers: requestHeaders
        )
        
        request.responseJSON { (dataResponce) in
            debugPrint(dataResponce)
            switch dataResponce.result {
            case .success(let value):
                let json = JSON(value)
                debugPrint(json)
                completion(json, nil)
            case .failure(let error):
                if let response = dataResponce.response, response.statusCode == 201 {   // Created
                    completion(nil, nil)
                } else if (error as NSError).code != -999 {   // The connection was cancelled.
                    completion(nil, error)
                }
            }
        }
    }
    
}
