//
//  SharedApiService.swift
//  IOS-101
//
//  Created by Artem Kireev on 16/02/2018.
//  Copyright © 2018 Artem Kireev. All rights reserved.
//

import Foundation


class SharedApiService: NSObject {
    
    static let shared: SharedApiService = SharedApiService()
    
    private(set) var articlesService: ArticleService
    
    private(set) var commentsService: CommentService
    
    private(set) var userInfoService: UserInfoService
    
    private(set) var userPhotoService: UserPhotoService
    
    private override init() {
        articlesService = ArticleService()
        commentsService = CommentService()
        userInfoService = UserInfoService()
        userPhotoService = UserPhotoService()
        super.init()
    }
    
}
